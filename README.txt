Apollo XIII
===========

This module provides a Views integration to follow your drupal.org issues.

# How to use

  * Edit the default view at: admin/structure/views/view/apollo_xiii_issue_view
  * Expand the "Advanced" tab and click on "Other > Query settings > Settings"
  * Configure the projects and issues you want to follow
  * Edit your View as you wish and save it
  * Enjoy...

# What you can do


# What you can't do


# Related projects

Some other projects also allow you to monitor
and follow your drupal.org projects and issues.

## Rocketship

A drupal.org issue scraper and display tool.
  * URL (sandbox): http://drupal.org/sandbox/goba/1470484

## Views XML Backend

Views XML Backend is a Views 3 plugin
that adds native XPath 1.0 query generation.
  * URL: http://drupal.org/project/views_xml_backend
